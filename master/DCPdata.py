class DCPd:
  unit_ip = '255.255.255.255'
  unit_id = 0
  unit_port = 2001
  alarm_temp = 'Clear'
  analog_temp = 0.00
  alarm_humd = 'Clear'
  analog_humd = 0.00
  is_active = False
  no_response = False

  def bch_cal(self, buff, count):
    BCH = 0x00
    nBCHpoly = 0x51
    fBCHpoly = 0x7f  
    for i in range(count):
      BCH = BCH ^ ord(buff[i])
      for j in range(8):
        if (BCH & 1) ==  1:
          BCH = (BCH >> 1) ^ nBCHpoly;
        else:
          BCH = BCH>>1
    BCH ^= fBCHpoly;
    return BCH;

  def init(self, ipa, did):
    self.unit_ip = ipa
    self.unit_id = int(did)
    if self.unit_ip == "255.255.255.255" or self.unit_id == 0:
      self.is_active = False
    else: 
      self.is_active = True

  def alarm_conv(self, data):
    if ord(data) & 0x08:
      return 'Major Over'
    elif ord(data) & 0x04:
      return 'Major Under'
    elif ord(data) & 0x02:
      return 'Minor Over'
    elif ord(data) & 0x01:
      return 'Minor Under'
    else:
      return 'Clear'
  
  def analog_conv(self, data):
    fnum = float(ord(data[1])*256+ord(data[0]))
    return fnum/100
  
  def validate(self, data):
    check = ''
    msg = ''
    if self.unit_id == ord(data[0]):
      check = data[0:2]
      if self.bch_cal(check, 2) == ord(data[2]):
        check = data[3:9]
        if self.bch_cal(check, 5) == ord(data[8]):
          if self.alarm_temp != self.alarm_conv(data[4]):
            self.alarm_temp = self.alarm_conv(data[4])
            msg = self.alarm_temp           
          self.analog_temp = self.analog_conv(data[6:8])
        else:
          print("read temperature failed")
          print(self.bch_cal(check, 5))
          print(ord(data[8]))
          return (False, '')
        check = data[9:15]
        if self.bch_cal(check, 5) == ord(data[14]):
          self.alarm_humd = self.alarm_conv(data[10])
          self.analog_humd = self.analog_conv(data[12:14])
        else:
          print("read humidity failed")
          print(self.bch_cal(check, 5))
          print(ord(data[14]))
          return (False, '')
      else:
        print("read DCP data failed")
        print(self.bch_cal(check, 2))
        print(ord(data[2]))
        return (False, '')
    else:
      print("wrong id")
      print(self.unit_id)
      print(ord(data[1]))
    return (True, msg)
       
      

