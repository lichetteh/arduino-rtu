import socket, time, threading
from flask import Flask, render_template, jsonify, request
from flask_mail import Mail, Message
from DCPdata import DCPd

app = Flask(__name__)
app.config.update(
  MAIL_PORT = 2025
)
mail = Mail(app)

connection_port = 2003
device_list = [DCPd() for i in range(8)]
sock=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(('', 2001))
INTERVAL = 10
wrongcs = False

def device_refresh():
  devices = [
    {
      'idx' : 0,
      'id': device_list[0].unit_id,
      'ip': device_list[0].unit_ip,
      'temperature': device_list[0].analog_temp,
      'humidity': device_list[0].analog_humd,
      'alarm': device_list[0].alarm_temp,
      'isactive': device_list[0].is_active
    },
    {
      'idx' : 1,
      'id': device_list[1].unit_id,
      'ip': device_list[1].unit_ip,
      'temperature': device_list[1].analog_temp,
      'humidity': device_list[1].analog_humd,
      'alarm': device_list[1].alarm_temp,
      'isactive': device_list[1].is_active
    },
    {
      'idx' : 2,
      'id': device_list[2].unit_id,
      'ip': device_list[2].unit_ip,
      'temperature': device_list[2].analog_temp,
      'humidity': device_list[2].analog_humd,
      'alarm': device_list[2].alarm_temp,
      'isactive': device_list[2].is_active
    },
    {
      'idx' : 3,
      'id': device_list[3].unit_id,
      'ip': device_list[3].unit_ip,
      'temperature': device_list[3].analog_temp,
      'humidity': device_list[3].analog_humd,
      'alarm': device_list[3].alarm_temp,
      'isactive': device_list[3].is_active
    },
    {
      'idx' : 4,
      'id': device_list[4].unit_id,
      'ip': device_list[4].unit_ip,
      'temperature': device_list[4].analog_temp,
      'humidity': device_list[4].analog_humd,
      'alarm': device_list[4].alarm_temp,
      'isactive': device_list[4].is_active
    },
    {
      'idx' : 5,
      'id': device_list[5].unit_id,
      'ip': device_list[5].unit_ip,
      'temperature': device_list[5].analog_temp,
      'humidity': device_list[5].analog_humd,
      'alarm': device_list[5].alarm_temp,
      'isactive': device_list[5].is_active
    },
    {
      'idx' : 6,
      'id': device_list[6].unit_id,
      'ip': device_list[6].unit_ip,
      'temperature': device_list[6].analog_temp,
      'humidity': device_list[6].analog_humd,
      'alarm': device_list[6].alarm_temp,
      'isactive': device_list[6].is_active
    },
    {
      'idx' : 7,
      'id': device_list[7].unit_id,
      'ip': device_list[7].unit_ip,
      'temperature': device_list[7].analog_temp,
      'humidity': device_list[7].analog_humd,
      'alarm': device_list[7].alarm_temp,
      'isactive': device_list[7].is_active
    }
  ]
  return devices

def test():
  device_list[0].init('192.168.1.33', 2)

def hexprint(data):
  for hbyte in data:
    print hex(ord(hbyte))+', ',
  print ' '

def poll():
  global wrongcs
  t_begin = time.clock()
  while True:    
    t_now = time.clock()
    t_pass = t_now - t_begin
    if t_pass > INTERVAL:
      t_begin = t_now
      for device in device_list:
        if device.is_active == True:
          POLL_KEY = chr(device.unit_id) + '\x03'
          if wrongcs == True:
            POLL_KEY = POLL_KEY + '\x01'
	  else:
            POLL_KEY = POLL_KEY + chr(device.bch_cal(POLL_KEY, 2))
          try:
            sock.sendto(POLL_KEY, (device.unit_ip, device.unit_port))
            print("sent data:", POLL_KEY)
            sock.settimeout(5.0)
            data1, addr = sock.recvfrom(1024)
            data2, addr = sock.recvfrom(1024)
            data3, addr = sock.recvfrom(1024)
            data = data1+data2+data3
            hexprint(data)
            print("ip: %s" % addr[0]),
            print(" port: %s" % addr[1])
            valid, alarm = device.validate(data)
            if valid == True:
              print("Status      : %s" % device.alarm_temp)
              print("Temperature : %s" % device.analog_temp)
            else:
              print("wrong data")
            if alarm != '':
              msg_str = "Alarm : %s Device : %d IP : %s" %(device.alarm_temp, device.unit_id, device.unit_ip)
              msg = Message(msg_str, sender="dps@server.net", recipients=["dps@master.net"])
              with app.app_context():
                mail.send(msg)
          except socket.timeout:
            print("no incoming data")
            device.no_response = True

@app.route('/setdevice', methods = ["POST"])
def setdevice():
  device_data = request.get_json(force=True)
  idx = device_data['idx']
  device_list[idx].init(device_data['ip'], device_data['id'])
  devices = device_refresh()
  return jsonify({'devices':devices})

@app.route('/wrongcs')
def forcecs():
  global wrongcs 
  wrongcs = True
  return "done"
   
@app.route('/refresh', methods = ["GET"])
def refresh():
  devices = device_refresh()
  return jsonify({'devices': devices})
        
@app.route('/')
def home():
  global wrongcs 
  wrongcs = False
  devices = device_refresh()
  return render_template("home.html", devices=devices)

def main():
  #test()
  threading.Thread(target=poll).start()
  app.run(debug=True, port=5000)

if __name__ == "__main__":
  main()
