#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include <SimpleDHT.h>
#include <DS3231_Simple.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <EEPROM.h>
#include <LiquidCrystal.h>


//============================================
//Definition for everything.
//pin definition
#define pana      0
#define prgb      7
#define nrgb      4
#define pdht      6
//buffer sizes
#define buffsz    40
#define tbuffsz   20
#define pbuffsz   6
//version
#define maj_ver   1
#define min_ver   1
#define revision  3
//refresh interval
#define read_time 5000
#define log_time  900000
//color value
#define c_hi      100
#define c_low     10
//key pad value
#define knothing  0
#define kup       1
#define kdown     2
#define kleft     3
#define kright    4
#define kenter    5
//EEPROM 
#define record_size 200
//alarm status
#define amjo      0x01
#define amno      0x02
#define amnu      0x04
#define amju      0x08      
//connection port
#define connection_port 2003
#define connection_time 5000

//============================================
//Token definations.
//error token is defined as FF, and token base 
//is E0.
#define terr  0xFF
#define tend  0xFE
#define tbase 0xE0
#define trgb  tbase
#define tset  trgb+1
#define thlp  tset+1
#define tsts  thlp+1
#define tver  tsts+1
#define tadd  tver+1
#define tsnr  tadd+1
#define time  tsnr+1
#define tlog  time+1
#define tipa  tlog+1

//============================================
//Global variables.
char buffer[buffsz];
byte tbuffer[tbuffsz];
char pbuffer[pbuffsz];
byte recidx = 0;
bool BCH_flag = false;
byte buffidx = 0;
byte menuidx = 0;
byte screenidx = 0;
byte historyidx = 0;
byte cursor_pos = 0;
int packetsz = 0;
byte udp_s = 0;
byte udp_r = 0;
int localport = 2001;
unsigned long blinkinterval = 300;
unsigned long blinkmillis = 0;
unsigned long readmillis = 0;
unsigned long logmillis = 0;
unsigned long lcdmillis = 0;
unsigned long checksum_timer = 0;
unsigned long timeout = 0;
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(nrgb, prgb, NEO_GRB);
SimpleDHT22 dht22;
DS3231_Simple Clock;
byte mac[] = { 0xEE, 0xAD, 0xBB, 0xEF, 0xFF, 0xED };
EthernetUDP Udp;
LiquidCrystal lcd(8, 9, 5, 4, 3, 2);
DateTime dnt;

//============================================
//Token table.
byte ttable[] = {
'R', 'G', 3, trgb, 
'S', 'E', 3, tset,
'H', 'E', 4, thlp,
'S', 'T', 6, tsts,
'V', 'E', 7, tver,
'A', 'D', 3, tadd,
'S', 'E', 6, tsnr,
'T', 'I', 4, time,
'H', 'I', 7, tlog,
'I', 'P', 2, tipa,
0};

//============================================
//State structures for each components.

struct RGB_{
 byte red;
 byte green;
 byte blue;
}RGB_, UDP_;

struct SNR_{
 float tmp;
 float hmd;
 float tmp_h = 0.0;
 float tmp_l = 500.0;
}SNR;

struct Thresh{
 float mjo = 90;
 float mno = 80;
 float mnu = 70;
 float mju = 60;
}alarm_thr;

struct IAP{
 byte ad1;
 byte ad2;
 byte ad3;
 byte ad4;
}ipa, sbm, gwy;

struct DCPheader{
 unsigned char unit_id = 2;
 unsigned char lines = 2;
 unsigned char BCH;
}dcp_head;

struct DCPdata{
 unsigned char display;
 unsigned char alarm = 0;
 unsigned char control = 0;
 unsigned char analog_low;
 unsigned char analog_high;
 unsigned char BCH;
}dcp_tdata, dcp_hdata;
  
//=============================================
//             Helper functions
//=============================================

//====================================================
//multiplies float by 100 and convert to 2 byte integer
//input: float
//output: short
short float_int(float num){
  num = num*100;
  return (short) num;    
}

//====================================================
//divides integer by 100 and convert to float
//input: short
//output: float
float int_float(short num){
  return (float) num/100;
}

//====================================================
//returns which button is pressed
//output: button pressed
byte button_press(){
  word val = analogRead(pana);
  if(val < 1000){
    Serial.println(val);
    Serial.println(menuidx);
    Serial.println(screenidx);
  }
  if(val < 100)
    return kleft;
  else if (val < 200)
    return kup;
  else if (val < 450)
    return kdown;
  else if (val < 600)
    return kright;
  else if (val < 800)
    return kenter;
  else 
    return knothing;
}

//====================================================
//prints times
void print_time(){
  Serial.print(dnt.Hour);
  Serial.print(F(":"));  
  Serial.print(dnt.Minute);
  Serial.print(F(":"));   
  Serial.print(dnt.Second);
  Serial.print(F(" ")); 
  Serial.print(dnt.Month);
  Serial.print(F("/")); 
  Serial.print(dnt.Day);
  Serial.print(F("/")); 
  Serial.print(dnt.Year);  
  Serial.println();
}

void read_sensor(){
  short snr;
  dht22.read2(pdht, &SNR.tmp, &SNR.hmd, NULL);
  SNR.tmp = (SNR.tmp*1.8)+32;
  snr = float_int(SNR.tmp);
  dcp_tdata.analog_low = snr & 0xFF;
  dcp_tdata.analog_high = snr>>8 & 0xFF;
  snr = float_int(SNR.hmd);
  dcp_hdata.analog_low = snr & 0xFF;
  dcp_hdata.analog_high = snr>>8 & 0xFF;
  if(SNR.tmp > SNR.tmp_h) SNR.tmp_h = SNR.tmp;
  else if(SNR.tmp < SNR.tmp_l) SNR.tmp_l = SNR.tmp;
}

void write_net(){
  EEPROM.put(record_size, ipa);
  EEPROM.put(record_size+4, sbm);
  EEPROM.put(record_size+8, gwy);
}

void read_net(){
  EEPROM.get(record_size, ipa);
  EEPROM.get(record_size+4, sbm);
  EEPROM.get(record_size+8, gwy);
}

void start_net(){
  IPAddress ip(ipa.ad1,ipa.ad2,ipa.ad3,ipa.ad4);
  IPAddress subnet(sbm.ad1, sbm.ad2, sbm.ad3, sbm.ad4);
  IPAddress gateway(gwy.ad1, gwy.ad2, gwy.ad3, gwy.ad4);
  Ethernet.begin(mac, ip, subnet, gateway);
}

//====================================================
//writes record of time and sensor values to eeprom
void write_record(){
  dnt = Clock.read();
  EEPROM.write(recidx++, dnt.Day);
  EEPROM.write(recidx++, dnt.Month);
  EEPROM.write(recidx++, dnt.Year);
  EEPROM.write(recidx++, dnt.Hour);
  EEPROM.write(recidx++, dnt.Minute);
  EEPROM.write(recidx++, dnt.Second);
  short snr = float_int(SNR.tmp);
  EEPROM.put(recidx, snr); recidx+=2;
  snr = float_int(SNR.hmd);
  EEPROM.put(recidx, snr); recidx+=2;
  if(recidx > record_size) recidx = 0;
}

//====================================================  
//reads and prints record of time and sensor values to eeprom
void read_record(){
  byte idx = 0;
  short snr;
  float fsnr;
  if(recidx == 0){
    Serial.println(F("no history"));
  }
  else{
    while(idx < recidx){
      dnt.Day = EEPROM.read(idx++);
      dnt.Month = EEPROM.read(idx++);
      dnt.Year = EEPROM.read(idx++);
      dnt.Hour = EEPROM.read(idx++);
      dnt.Minute = EEPROM.read(idx++);
      dnt.Second = EEPROM.read(idx++);
      print_time();
      EEPROM.get(idx, snr); idx+=2;
      fsnr = int_float(snr);
      Serial.print(F("temperature : "));
      Serial.println(fsnr);
      EEPROM.get(idx, snr); idx+=2;
      fsnr = int_float(snr);
      Serial.print(F("temperature : "));
      Serial.println(fsnr);
    }
  }
  Serial.print(F("lowest temperature: "));
  Serial.println(SNR.tmp_l);
  Serial.print(F("highest temperature: "));
  Serial.println(SNR.tmp_h);
}

//====================================================
//calculate bch value.
byte BCH_cal(char buff[], byte count){
  byte i, j, BCH, nBCHpoly, fBCHpoly;

  nBCHpoly = 0x51;
  fBCHpoly = 0x7f;
  
  for(BCH = 0, i = 0; i < count-1; i++){
    BCH ^= buff[i];
    for(j = 0; j < 8; j++){
      if((BCH & 1) ==  1)
        BCH = (BCH >> 1) ^ nBCHpoly;
      else BCH >>= 1;
    }
  }
  BCH ^= fBCHpoly;
  
  return (BCH);
}
  

//===================================================
//response to dcp via udp
void dcp_response(){
  if(Udp.beginPacket(Udp.remoteIP(), localport)){
    udp_s++;
    pbuffer[0] = dcp_head.unit_id;
    pbuffer[1] = dcp_head.lines; 
    dcp_head.BCH = BCH_cal(pbuffer, 3);
    pbuffer[2] = dcp_head.BCH;
    Udp.write(pbuffer, 3);
    Udp.endPacket();
  }
  if(Udp.beginPacket(Udp.remoteIP(), localport)){
    dcp_tdata.display = 1;
    pbuffer[0] = dcp_tdata.display;
    dcp_tdata.control |= 0x80;
    pbuffer[1] = dcp_tdata.alarm;
    pbuffer[2] = dcp_tdata.control;
    pbuffer[3] = dcp_tdata.analog_low;
    pbuffer[4] = dcp_tdata.analog_high;
    dcp_tdata.BCH = BCH_cal(pbuffer, 6);
    pbuffer[5] = dcp_tdata.BCH; 
    Udp.write(pbuffer, 6);
    Udp.endPacket();
  }
  if(Udp.beginPacket(Udp.remoteIP(), localport)){
    dcp_hdata.display = 2;
    pbuffer[0] = dcp_hdata.display;
    dcp_hdata.control |= 0x80;
    pbuffer[1] = dcp_hdata.alarm;
    pbuffer[2] = dcp_hdata.control;
    pbuffer[3] = dcp_hdata.analog_low;
    pbuffer[4] = dcp_hdata.analog_high;
    dcp_hdata.BCH = BCH_cal(pbuffer, 6);
    pbuffer[5] = dcp_hdata.BCH; 
    Udp.write(pbuffer, 6);
    Serial.println("sent");
    Udp.endPacket();
  }
}

//====================================================
//checks if the dcp poll data is valid
void dcp_check(){
  if(packetsz == 3){
    if(pbuffer[0] == dcp_head.unit_id){
      if(BCH_cal(pbuffer, 3)== pbuffer[2]){
        if(pbuffer[1] == 3)
          dcp_response();
      }
      else{
        BCH_flag = true;
        checksum_timer = millis();
      }
    }
  }
}

//====================================================
//checks for the incoming packet is a refresh for connection
void connection_check(){
  if(pbuffer[0] == 0xFF){
    timeout = millis();
    UDP_.red = c_low;
    UDP_.green = c_hi;
    UDP_.blue = c_low;
  }
}

//====================================================
//listens and recieves udp and stores into buffer
void udp_listen(){
  if((packetsz = Udp.parsePacket())) {
    Udp.read(pbuffer, pbuffsz);
    pbuffer[packetsz] = '\0';
    if(packetsz == 1)
      connection_check();
    else{
      dcp_check();
      udp_r++;
    }
  }
}

//==================================================================
//checks for temperature change and sets alarm and led accordingly 
void tmp_check(){
  if(SNR.tmp >=alarm_thr.mnu && SNR.tmp <= alarm_thr.mno){
    dcp_tdata.alarm &= 0x00;   
    pixels.setPixelColor(1, pixels.Color(c_low, c_hi, c_low));
  }
  else {
    if(SNR.tmp < alarm_thr.mju){
      dcp_tdata.alarm &= 0x00; 
      dcp_tdata.alarm |= amju;
      pixels.setPixelColor(1, pixels.Color(c_hi, c_low, c_hi));
    }
    else if(SNR.tmp > alarm_thr.mjo){
      dcp_tdata.alarm &= 0x00;
      dcp_tdata.alarm |= amjo;
      pixels.setPixelColor(1, pixels.Color(c_hi, c_low, c_low));
    }
    else if(SNR.tmp < alarm_thr.mnu){
      dcp_tdata.alarm &= 0x00;
      dcp_tdata.alarm |= amnu;
      pixels.setPixelColor(1, pixels.Color(c_low, c_low, c_hi));
    }
    else if(SNR.tmp > alarm_thr.mno){
      dcp_tdata.alarm &= 0x00;
      dcp_tdata.alarm |= amno;
      pixels.setPixelColor(1, pixels.Color(c_hi, c_hi, c_low));
    }   
  }
  pixels.show();
}

//=============================================
//Print out the token buffer.
void print_tokens(){
  byte idx = 0;
  while(tbuffer[idx] != tend){
    if(idx == 0) Serial.print(F("tokens : ")); 
    Serial.print(tbuffer[idx++], HEX);
    Serial.print(F(" "));
  }
  if(idx>0){
  Serial.println(F(" "));
  }
}

//=============================================
//Print out the help table
void print_help(){
  Serial.println(F("Command Help:"));
  Serial.println(F("RGB # # #   : set color for rgb led."));
  Serial.println(F("ADD # #     : sum 2 given numbers."));
  Serial.println(F("STATUS      : show board status."));
  Serial.println(F("VERSION     : show current version."));
  Serial.println(F("TIME        : show current time."));
  Serial.println(F("SET TIME #hour #minute #mm #dd #yy"));
  Serial.println(F("SENSOR      : show current temperature and humidity."));
  Serial.println(F("HISTORY     : show up to 20 log history"));
  Serial.println(F("HELP        : show this menu again."));
}

//=============================================
//View the states of each component and print out
//their status respectively.
void print_status(){
  Serial.print(F("RGB : "));
  if(RGB_.red == 0 && RGB_.green == 0 && RGB_.blue == 0)
      Serial.println(F("off"));
  else
      Serial.println(F("on"));
  Serial.print(F("temperature : (current) "));
  Serial.print(SNR.tmp);
  Serial.print(F("  (high) "));
  Serial.print(SNR.tmp_h);
  Serial.print(F("  (low) "));
  Serial.println(SNR.tmp_l);
  Serial.print(F("humidity : "));
  Serial.println(SNR.hmd);
  dnt = Clock.read();
  print_time();
}

//==============================================
//            Parser function
//==============================================
//this function reads the token buffer and interprets 
//tokens into commands. If no valid tokens found, an error
//message will be printed instead.
void parse(){
  byte idx = 0;

  while(tbuffer[idx] != tend){
    if(tbuffer[idx] == terr) idx++;
    else break;
  }
  if(tbuffer[idx] == trgb){
    if(tbuffer[idx+2] < tbase){
      RGB_.red = ((tbuffer[idx+2] << 8) + tbuffer[idx+1]) % 256;
      if(tbuffer[idx+4] < tbase){
        RGB_.green = ((tbuffer[idx+4] << 8) + tbuffer[idx+3]) % 256;
	if(tbuffer[idx+6] < tbase){
          RGB_.blue = ((tbuffer[idx+6] << 8) + tbuffer[idx+5]) % 256;
          pixels.setPixelColor(0, pixels.Color(RGB_.red, RGB_.green, RGB_.blue));
        }
      }
    }
  }
  else if(tbuffer[idx] == tlog){
    read_record();
  }
  else if(tbuffer[idx] == time){
    dnt = Clock.read();
    Serial.print(F("time now is "));
    print_time();
  }
  else if(tbuffer[idx] == tsnr){
    read_sensor();
    Serial.print(F("temperature : "));
    Serial.println(SNR.tmp);
    Serial.print(F("humidity : "));
    Serial.println(SNR.hmd);
  }
  else if(tbuffer[idx] == tset){
    if(tbuffer[idx+1] == time){
      if(tbuffer[idx+3] < tbase){
        if(tbuffer[idx+5] < tbase){
          if(tbuffer[idx+7] < tbase){
            if(tbuffer[idx+9] < tbase){
              if(tbuffer[idx+11] < tbase){
                dnt.Hour = ((tbuffer[idx+3] << 8) + tbuffer[idx+2]);
                dnt.Minute = ((tbuffer[idx+5] << 8) + tbuffer[idx+4]);
                dnt.Month = ((tbuffer[idx+7] << 8) + tbuffer[idx+6]);
                dnt.Day = ((tbuffer[idx+9] << 8) + tbuffer[idx+8]);
                dnt.Year = ((tbuffer[idx+11] << 8) + tbuffer[idx+10]);
                dnt.Second = 0;
                Clock.write(dnt);
                Serial.print(F("time has been set to: "));
                print_time();
              }
            }
          }
        }
      }
    }    
  }
  else if(tbuffer[idx] == tipa){
    if(tbuffer[idx+2] < tbase){
      ipa.ad1 = ((tbuffer[idx+2] << 8) + tbuffer[idx+1]) % 256;
      if(tbuffer[idx+4] < tbase){
        ipa.ad2 = ((tbuffer[idx+4] << 8) + tbuffer[idx+3]) % 256;
	if(tbuffer[idx+6] < tbase){
          ipa.ad3 = ((tbuffer[idx+6] << 8) + tbuffer[idx+5]) % 256;
          if(tbuffer[idx+8] < tbase){
            ipa.ad4 = ((tbuffer[idx+8] << 8) + tbuffer[idx+6]) % 256;
            start_net();
          }
        }
      }
    }
  }
  else if(tbuffer[idx] == thlp){
    print_help();
  }
  else if(tbuffer[idx] == tver){
    Serial.print(F("version "));
    Serial.print(maj_ver);
    Serial.print(F("."));
    Serial.print(min_ver);
    Serial.print(F("."));
    Serial.println(revision);
  }
  else if(tbuffer[idx] == tadd){
    int sum = 0;
    if(tbuffer[idx+2] < tbase){
      sum = sum + ((tbuffer[idx+2] << 8) + tbuffer[idx+1]);
      if(tbuffer[idx+4] < tbase){
        sum = sum + ((tbuffer[idx+4] << 8) + tbuffer[idx+3]);
        Serial.print(F("sum is "));
        Serial.println(sum);
      }
    }
  }
  else if(tbuffer[0] != tend){
    Serial.println(F("Error input, enter HELP to see command menu."));
  }
}

//==============================================
//         tokenization function
//==============================================
//This function compares each word in the input
//buffer with the token table, and converts them 
//into a token table. This function will call the 
//parser function before termination.
void tokenize(){
  byte sidx = 0;
  byte eidx;
  byte bidx = 0;

  while(buffer[sidx] != '\0'){
    byte tidx = 0;
    word numbr = 0;
    bool found = false;
    if(buffer[sidx] == ' '){
      sidx++;
    }
    else{
      eidx = sidx;
      while(buffer[eidx] != ' ' && buffer[eidx] != '\0'){
        eidx++;
      }
      if(eidx-sidx > 1){
        while(ttable[tidx] != 0){
          if((buffer[sidx] == ttable[tidx]) &&
             (buffer[sidx+1] == ttable[tidx+1]) &&
             (eidx-sidx == ttable[tidx+2])){
             tbuffer[bidx++] = ttable[tidx+3];
             found = true;
             break;
          }
          tidx+=4;
        }
      }
      if(found == false){
         tidx = sidx;
         while(tidx < eidx){
           if(buffer[tidx] < '0' || buffer[tidx] > '9'){
             numbr = 0xFFFF;
             break;
           }
           else
             numbr = numbr*10 + (buffer[tidx++] - '0');
         }
         if((numbr) < 0xE000){
           tbuffer[bidx++] = numbr & 0x00FF;
           tbuffer[bidx++] = (numbr >> 8) & 0x00FF;
         }
         else{
           tbuffer[bidx++] = terr;
         }
      }
      sidx = eidx;
    }
  }
  tbuffer[bidx] = tend;
  parse();
}

//=================================================
//                Read function
//=================================================
//this function will take each keyboard input and 
//store them in the buffer. It will call the tokenize
//function when the Return key is inserted.
void read(){
  char readc;
  
  readc = Serial.read();
  if(readc >= 32 && readc < 127 && buffidx < buffsz-1){
    buffer[buffidx++] = toupper(readc);
    Serial.print(readc);
  }
  else if(readc == 13){
    buffer[buffidx] = '\0';
    Serial.println();
    Serial.println(buffer);
    tokenize();
    print_tokens();
    buffidx = 0;
    Serial.print("CM> ");
  }
  else if(readc == 127 && buffidx>0){
    buffidx--;
    Serial.print("\b \b");
  }
}

//====================================================
//               LCD input function
//====================================================
//this function takes a key_pad command and changes the
//lcd screen contents.
void lcd_key(byte key_cmd){
  if(menuidx == 0){
    if(key_cmd == kenter)
      menuidx = 1;
  }
  else if(menuidx == 1){
    if(key_cmd == kright)
      screenidx = (screenidx + 1)%4;
    else if(key_cmd == kleft)
      screenidx = (screenidx + 3)%4;
    else if(key_cmd == kenter && screenidx == 3){
      menuidx = 2;
      screenidx = 0;
    }
    else if(key_cmd == kenter)
      menuidx = 0;
  }
  else if(menuidx == 2){
    if(key_cmd == kright)
      screenidx = (screenidx + 1)%4;
    else if(key_cmd == kleft)
      screenidx = (screenidx + 3)%4;
    else if(key_cmd == kenter && screenidx == 0){
      menuidx = 3;
      screenidx = 0;
    }
    else if(key_cmd == kenter && screenidx == 1){
      menuidx = 4;
      screenidx = 0;
    }
    else if(key_cmd == kenter && screenidx == 2){
      menuidx = 5;
      screenidx = 0;
    }
    else if(key_cmd == kenter && screenidx == 3){
      menuidx = 0;
      screenidx = 0;
    }
  }
  else if(menuidx == 3){
    if(screenidx == 0){
      if(key_cmd == kenter){
        menuidx=3;
        screenidx=1;
      }
      else if(key_cmd == kright)
        cursor_pos = (cursor_pos + 1)%4;
      else if(key_cmd == kleft)
        cursor_pos = (cursor_pos + 3)%4;
      else if(key_cmd == kup){
        if(cursor_pos == 0) ipa.ad1++;
        else if(cursor_pos == 1) ipa.ad2++;
        else if(cursor_pos == 2) ipa.ad3++;
        else if(cursor_pos == 3) ipa.ad4++;
      }
      else if(key_cmd == kdown){
        if(cursor_pos == 0) ipa.ad1--;
        else if(cursor_pos == 1) ipa.ad2--;
        else if(cursor_pos == 2) ipa.ad3--;
        else if(cursor_pos == 3) ipa.ad4--;
      }
    }
    else if(screenidx == 1){
      if(key_cmd == kenter){
        menuidx=3;
        screenidx=2;
      }
      else if(key_cmd == kright)
        cursor_pos = (cursor_pos + 1)%4;
      else if(key_cmd == kleft)
        cursor_pos = (cursor_pos + 3)%4;
      else if(key_cmd == kup){
        if(cursor_pos == 0) sbm.ad1++;
        else if(cursor_pos == 1) sbm.ad2++;
        else if(cursor_pos == 2) sbm.ad3++;
        else if(cursor_pos == 3) sbm.ad4++;
      }
      else if(key_cmd == kdown){
        if(cursor_pos == 0) sbm.ad1--;
        else if(cursor_pos == 1) sbm.ad2--;
        else if(cursor_pos == 2) sbm.ad3--;
        else if(cursor_pos == 3) sbm.ad4--;
      }
    }
    else if(screenidx == 2){
      if(key_cmd == kenter){
        menuidx = 2;
        screenidx = 0;
        write_net();
        start_net();
      }
      else if(key_cmd == kright)
        cursor_pos = (cursor_pos + 1)%4;
      else if(key_cmd == kleft)
        cursor_pos = (cursor_pos + 3)%4;
      else if(key_cmd == kup){
        if(cursor_pos == 0) gwy.ad1++;
        else if(cursor_pos == 1) gwy.ad2++;
        else if(cursor_pos == 2) gwy.ad3++;
        else if(cursor_pos == 3) gwy.ad4++;
      }
      else if(key_cmd == kdown){
        if(cursor_pos == 0) gwy.ad1--;
        else if(cursor_pos == 1) gwy.ad2--;
        else if(cursor_pos == 2) gwy.ad3--;
        else if(cursor_pos == 3) gwy.ad4--;
      }
    }
  }
  else if(menuidx == 4){
    if(key_cmd == kenter){
      menuidx = 2;
      screenidx = 1;
    }
    else if(key_cmd == kright){
      cursor_pos = (cursor_pos + 1)%4;
    }
    else if(key_cmd == kleft){
      cursor_pos = (cursor_pos + 3)%4;
    }
    else if(key_cmd == kup){
        if(cursor_pos == 0) alarm_thr.mjo++;
        else if(cursor_pos == 1) alarm_thr.mno++;
        else if(cursor_pos == 2) alarm_thr.mju++;
        else if(cursor_pos == 3) alarm_thr.mnu++;
    }
    else if(key_cmd == kdown){
        if(cursor_pos == 0) alarm_thr.mjo--;
        else if(cursor_pos == 1) alarm_thr.mno--;
        else if(cursor_pos == 2) alarm_thr.mju--;
        else if(cursor_pos == 3) alarm_thr.mnu--;
    }
  }
  else if(menuidx == 5){
    if(key_cmd == kenter){
      menuidx = 2;
      screenidx = 2;
    }
    recidx = 0;
  }
}

//====================================================
//                    LCD menu
//====================================================
//this function updates and displays the lcd contents
void lcd_menu(){
  lcd.clear();
  if(BCH_flag == true){
    unsigned long currentmillis = millis();
    if(currentmillis - checksum_timer > 5000){
      BCH_flag = false;
    }
    lcd.print(F("checksum error"));
    lcd.setCursor(0, 1);
    lcd.print(F("act:"));
    lcd.print(int(pbuffer[2]));
    lcd.print(" ");
    lcd.print(F("exp:"));
    lcd.print(BCH_cal(pbuffer, 3));
  }
  else if(menuidx == 0){
    if(screenidx == 0){     
      lcd.print(F("tempt : "));
      lcd.print(SNR.tmp);
      lcd.setCursor(0, 1);
      lcd.print(F("humid : "));
      lcd.print(SNR.hmd);
    }
    else if(screenidx == 1){
      unsigned long currentmillis = millis();
      float ftmp;
      float fhmd;
      short snr;
      if(recidx > 0){
        if(currentmillis - lcdmillis > 3000){
          historyidx = (historyidx)%recidx;
          dnt.Day = EEPROM.read(historyidx++);
          dnt.Month = EEPROM.read(historyidx++);
          dnt.Year = EEPROM.read(historyidx++);
          dnt.Hour = EEPROM.read(historyidx++);
          dnt.Minute = EEPROM.read(historyidx++);
          dnt.Second = EEPROM.read(historyidx++);
          print_time();
          EEPROM.get(historyidx, snr); historyidx+=2;
          ftmp = int_float(snr);
          Serial.print(F("temperature : "));
          Serial.println(ftmp);
          EEPROM.get(historyidx, snr); historyidx+=2;
          fhmd = int_float(snr);
          Serial.print(F("temperature : "));
          Serial.println(fhmd);
        }
        lcd.print(F("t:"));
        lcd.print(ftmp);
        lcd.print(F(" h:"));
        lcd.print(fhmd);
        lcd.setCursor(0, 1);
        lcd.print(dnt.Hour);
        lcd.print(F(":"));  
        lcd.print(dnt.Minute);
        lcd.print(F(":"));   
        lcd.print(dnt.Second);
        lcd.print(F(" ")); 
        lcd.print(dnt.Month);
        lcd.print(F("/")); 
        lcd.print(dnt.Day);
        lcd.print(F("/")); 
        lcd.print(dnt.Year);
      }
      else
      lcd.print(F("no history"));  
    }
    else if(screenidx == 2){
      lcd.print(F("packet sent: "));
      lcd.print(udp_s, DEC);
      lcd.setCursor(0, 1);
      lcd.print(F("packet rec: "));
      lcd.print(udp_r, DEC);
    }
  }
  else if(menuidx == 1){
    if(screenidx == 0)
      lcd.print(F("Home"));
    else if(screenidx == 1)
      lcd.print(F("History"));
    else if(screenidx == 2)
      lcd.print(F("Stats"));
    else if(screenidx == 3)
      lcd.print(F("Settings"));
  }
  else if(menuidx == 2){
    if(screenidx == 0){
      lcd.print(F("Ethernet"));
    }
    else if(screenidx == 1){
      lcd.print(F("Sensor Thresold"));
    }
    else if(screenidx == 2){
      lcd.print(F("Delete History"));
    } 
    else if(screenidx == 3){
      lcd.print(F("Home"));
    }
  }
  else if(menuidx == 3){
    if(screenidx == 0){
      lcd.print(F("IP: "));
      lcd.setCursor(0,1);
      lcd.print(ipa.ad1, DEC);
      lcd.setCursor(3,1);
      lcd.print(F("."));
      lcd.print(ipa.ad2, DEC);
      lcd.setCursor(7,1);
      lcd.print(F("."));
      lcd.print(ipa.ad3, DEC);
      lcd.setCursor(11,1);
      lcd.print(F("."));
      lcd.print(ipa.ad4, DEC);
      lcd.setCursor(10, 0);
      if(cursor_pos == 0)
      lcd.setCursor(0, 1);
    else if(cursor_pos == 1)
      lcd.setCursor(4, 1);
    else if(cursor_pos == 2)
      lcd.setCursor(8, 1);
    else if(cursor_pos == 3)
      lcd.setCursor(12, 1);
    }
    else if(screenidx == 1){
      lcd.print(F("Subnet Mask: "));
      lcd.setCursor(0,1);
      lcd.print(sbm.ad1, DEC);
      lcd.setCursor(3,1);
      lcd.print(F("."));
      lcd.print(sbm.ad2, DEC);
      lcd.setCursor(7,1);
      lcd.print(F("."));
      lcd.print(sbm.ad3, DEC);
      lcd.setCursor(11,1);
      lcd.print(F("."));
      lcd.print(sbm.ad4, DEC);
      lcd.setCursor(10, 0);
      lcd.print(cursor_pos, DEC);
      if(cursor_pos == 0)
      lcd.setCursor(0, 1);
    else if(cursor_pos == 1)
      lcd.setCursor(4, 1);
    else if(cursor_pos == 2)
      lcd.setCursor(8, 1);
    else if(cursor_pos == 3)
      lcd.setCursor(12, 1);
    }
    else if(screenidx == 2){
      lcd.print(F("Gateway: "));
      lcd.setCursor(0,1);
      lcd.print(gwy.ad1, DEC);
      lcd.setCursor(3,1);
      lcd.print(F("."));
      lcd.print(gwy.ad2, DEC);
      lcd.setCursor(7,1);
      lcd.print(F("."));
      lcd.print(gwy.ad3, DEC);
      lcd.setCursor(11,1);
      lcd.print(F("."));
      lcd.print(gwy.ad4, DEC);
      lcd.setCursor(10, 0);
      lcd.print(cursor_pos, DEC);
      if(cursor_pos == 0)
      lcd.setCursor(3, 1);
    else if(cursor_pos == 1)
      lcd.setCursor(4, 0);
    else if(cursor_pos == 2)
      lcd.setCursor(8, 1);
    else if(cursor_pos == 3)
      lcd.setCursor(12, 1);
    }
  }
  else if(menuidx == 4){
    lcd.print(F("Mjo:"));
    lcd.print(alarm_thr.mjo, DEC);
    lcd.setCursor(9, 0);
    lcd.print(F("Mno:"));
    lcd.print(alarm_thr.mno, DEC);
    lcd.setCursor(0, 1);
    lcd.print(F("Mju:"));
    lcd.print(alarm_thr.mju, DEC);
    lcd.setCursor(9, 1);
    lcd.print(F("Mnu:"));
    lcd.print(alarm_thr.mnu, DEC);
    if(cursor_pos == 0)
      lcd.setCursor(3, 0);
    else if(cursor_pos == 1)
      lcd.setCursor(12, 0);
    else if(cursor_pos == 2)
      lcd.setCursor(3, 1);
    else if(cursor_pos == 3)
      lcd.setCursor(12, 1);
  }
  else if(menuidx == 5){
    lcd.print(F("History Erased"));
  }
}

//==================================================
//refreshes the board components.
void board_refresh(){
  unsigned long currentmillis = millis();
  if(currentmillis - blinkmillis >= blinkinterval){
    blinkmillis = currentmillis; 
    lcd_key(button_press());
    lcd_menu(); 
  }
  if(currentmillis - readmillis >= read_time){
    readmillis = currentmillis;
    read_sensor();
    tmp_check();
  }
  if(currentmillis - timeout >= connection_time){
    UDP_.red = c_hi;
    UDP_.green = c_low;
    UDP_.blue = c_low;
  }
  if(currentmillis - logmillis >= log_time){
    logmillis = currentmillis;
    write_record();
  }
  pixels.setPixelColor(3, pixels.Color(UDP_.red, UDP_.green, UDP_.blue));
  pixels.show();
}

void setup(){
  Serial.begin(115200);
  Clock.begin();
  pixels.begin();
  pixels.setPixelColor(2, pixels.Color(c_low, c_hi, c_low));
  read_net();
  delay(750);
  start_net();
  Udp.begin(localport);
  Udp.begin(connection_port);
  analogWrite(A3, 120);
  lcd.begin(16, 2);
  Serial.print("CM> ");
}


//Loop function
void loop(){
  if(Serial.available()>0){
    read();    
  }
  board_refresh();
  udp_listen();
  board_refresh(); 
}



